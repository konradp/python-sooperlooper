state = {
  'count_pads': 8,
  'is_initializing': False,
  'last_ping': None,
  'mute_mode': False,
  'pad_selected': 0,
  'midi': {
    'dev_in': 'arturia',
    'dev_out': 'arturia',
    'devs_in': [],
    'devs_out': [],
    'msg': '',
    'status': 'Not connected',
  },
  'osc': {
    'status': 'Not connected',
  },
}
