from aiohttp import web
from datetime import datetime, timedelta
from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient
import asyncio
import json
import mido
import sys
import threading as th
import time
import websockets

# Local imports
from arturia_colors import ArturiaColors
from browser import Browser
from config import config
from http_routes import routes
from state import state
from util import RepeatTimer


DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'
DURATION_LONG_PRESS = 0.5
arturia_colors = None
osc_client = None
WS_CONNECTIONS = set()
browser = None

# STATE VARIABLES
# TODO: Move them to state var? or config?
COUNT_PADS_INIT = 5
state_pads = [False]*state['count_pads']
pad_timers = [None]*state['count_pads']


def usage():
  # TODO: using mido here crashes if alsa is not running
  # TODO: (maybe): remove this arg and allow the app to start without it
  # TODO: (maybe): default settings can be saved in a config file, e.g. which
  #                MIDI device to auto connect to
  print('Usage: python3 app.py')
  exit(1)


##### Handlers: Websocket
async def ws_handler(websocket):
  global browser
  global config
  global state
  global COUNT_PADS_INIT
  browser.WS_CONNECTIONS.add(websocket)
  WS_CONNECTIONS.add(websocket)
  osc_local_addr = config['osc_local']['addr']

  async for msg in websocket:
    # Handle each WS message
    msg = json.loads(msg)
    print('WS RECV:', msg)
    if msg['type'] == 'test':
      # TEST
      osc_loop_get(-1, 'state', osc_local_addr, '/default')
    elif msg['type'] == 'init':
      init_loops()
    elif msg['type'] == 'get_loops_state':
      for pad in range(state['count_pads']):
        print('PAD:', pad)
        osc_loop_get(pad, 'state', osc_local_addr, '/loop_get')
    elif msg['type'] == 'get_status':
      refresh_status()
      #osc_send_ping()
    elif msg['type'] == 'get_pad_count':
      osc_send_ping_get_loop_count()
    #elif msg['type'] == 'midi_refresh':
    #  midi_refresh()
    elif msg['type'] == 'midi_set_in':
      midi_set_in(msg['value'])
    elif msg['type'] == 'midi_set_out':
      midi_set_out(msg['value'])
    elif msg['type'] == 'osc_test':
      # Expected message:
      # {
      #   'type': 'osc_test',
      #   'loop_id': LOOP_ID,
      #   'cmd': CMD,
      #   'params': [ PARAM1, PARAM2, ... ]
      # }
      # Example: wss.send(JSON.stringify({'type': 'osc_test', 'loop_id': 2, 'cmd': 'get', 'params': [ 'state', 'localhost:9000', '/default' ]}))
      # Send OSC message directly
      osc_loop_test(msg['loop_id'], msg['cmd'], msg['params'])



##### OSC SEND HANDLERS ###########
# Global methods

def osc_check_status():
  # Send a ping to OSC, and check when was latest received ping
  osc_send_ping()
  # Check last received ping
  last_ping = state['last_ping']
  old_status = state['osc']['status']
  if last_ping is None \
    or last_ping < datetime.today() - timedelta(seconds=2):
    state['osc']['status'] = 'Not connected'
  else:
    state['osc']['status'] = 'Connected'
  if state['osc']['status'] != old_status:
    print('update status')
    browser.send_status(state)

def osc_send_ping():
  osc_client.send_message('/ping', [
    config['osc_local']['addr'],
    '/ping'
  ])
def osc_send_ping_get_loop_count():
  osc_client.send_message('/ping', [
    config['osc_local']['addr'],
    '/ping_get_loop_count'
  ])
def osc_get(param, return_url=None, retpath=None):
  if return_url is None:
    return_url = config['osc_local']['addr']
  if retpath is None:
    retpath = '/default'
  osc_client.send_message('/get', [
    param,
    return_url,
    retpath,
  ])
def osc_set(param, value):
  osc_client.send_message('/set', [ param, value ])


# Loop methods
def osc_loop_test(loop_id, cmd, params):
  osc_client.send_message(f'/sl/{loop_id}/{cmd}', params)
def osc_loop_get(loop_id, control, return_url, return_path):
  osc_client.send_message(f'/sl/{loop_id}/get', [
    control,
    return_url,
    return_path,
  ])
def osc_loop_set(loop_id, control, value):
  osc_client.send_message(f'/sl/{loop_id}/set', [
    control,
    value,
  ])
def osc_loop_hit(loop_id, cmd):
  osc_client.send_message(f'/sl/{loop_id}/hit', cmd)
def osc_loop_del():
  osc_client.send_message('/loop_del', -1)
def osc_loop_add():
  osc_client.send_message('/loop_add', [1, 47.55])
def osc_register_loop_count_updates():
  osc_client.send_message('/register', [
    config['osc_local']['addr'],
    '/ping_get_loop_count'
  ])


##### Handlers: OSC RECV
def osc_recv_ping(address, *args):
  # PING gets status
  global state
  state['last_ping'] = datetime.today()


def osc_recv_loop_count(address, *args):
  # PING gets loop count
  global state
  global browser
  print(f"OSC RECV: {address}: {args}")
  state['count_pads'] = args[2]
  browser.set_loop_count(args[2])


def osc_recv_loop_get(address, *args):
  global browser
  print(f'OSC RECV: {address}: {args}')
  if args[1] == 'state':
    loop_id = args[0]
    loop_state = args[2]
  elif args[1] == 'selected_loop_num':
    loop_id = int(args[2])
    browser.select_pad(loop_id+1)


def osc_recv_default(address, *args):
  print(f"OSC DEFAULT HANDLER: {address}: {args}")


##### Handlers: MIDI ############
def handle_midi_msg(msg):
  global arturia_colors
  global browser
  global state
  print('MIDI MSG:', msg)
  if msg.type == 'note_on' or \
    msg.type == 'note_off':
    pad = msg.note - 35 - 1 # 0, 1, 2
    if 0 <= pad <= 7 \
      and (msg.type in ['note_on', 'note_off' ]):
      if msg.type == 'note_on':
        pad_timers[pad] = th.Timer(DURATION_LONG_PRESS,
          check_longpress, None, { 'pad': pad }
        )
        pad_timers[pad].start()
        state_pads[pad] = True
        browser.touch_pad(pad+1)
      elif msg.type == 'note_off':
        print(f'MIDI: pad select: {pad}')
        if pad_timers[pad] is not None:
          pad_timers[pad].cancel()
        state_pads[pad] = False
        arturia_colors.reset_pad_colors()
        arturia_colors.set_pad_color(pad, 'blue')
        osc_set('selected_loop_num', pad)
        state['pad_selected'] = pad
        browser.select_pad(pad+1)
  elif msg.type == 'control_change':
    # sustain pedal, pads 9-16
    print('CONTROL_CHANGE', msg)
    if msg.control == 64 and msg.value == 0:
      # Sustain pedal off
      print('SELECTED', state)
      pad_selected = state['pad_selected']
      osc_loop_hit(pad_selected, 'record')
    elif 22 <= msg.control <= 29 and msg.value == 0:
      print('MUTE PAD PRESSED', msg.control-22)
    # PEDAL ON:  control_change channel=0 control=64 value=127 time=0
    # PEDAL OFF: control_change channel=0 control=64 value=0 time=0
    # PAD 1 ON:  control_change channel=0 control=22 value=127 time=0
    # PAD 1 OFF: control_change channel=0 control=22 value=0 time=0
    # PAD 2: control 23

  elif msg.type == 'polytouch':
    # Pressing the pad harder and lighter
    pass
  elif msg.type == 'sysex':
    # e.g. the 'shift' key
    # key: Pad 1-8 / 9-16
    #   sysex data=(0,32,107,127,66,2,0,0,47,127) time=0 # ON
    #   sysex data=(0,32,107,127,66,2,0,0,47,0) time=0   # OFF
    if msg.data == (0,32,107,127,66,2,0,0,47,0):
      # MUTE: key 'Pad 1-8 / 9-16' depressed
      print('switch between mute/unmute')
      state['mute_mode'] = not state['mute_mode']
      arturia_colors.mode_mute()
      browser.mode_mute()
    print(msg)
  else:
    print('ERR: Unhandled msg type:', msg)



################# APP METHODS ##################
def check_longpress(pad):
  if state_pads[pad]:
    print('long press on', pad)


def init_loops():
  global state
  # Initialize session: delete and create loops
  state['is_initializing'] = True # TODO: Not required
  # Set quantize = loop
  for loop in range(state['count_pads']):
    # Delete loops
    print('DELETE LOOP', loop)
    osc_loop_del()
    time.sleep(0.5)
  osc_send_ping_get_loop_count()
  for pad in range(COUNT_PADS_INIT):
    print('CREATE LOOP', pad)
    osc_loop_add()
  osc_send_ping_get_loop_count()
  time.sleep(2)
  osc_loop_set(-1, 'quantize', 3)
  osc_loop_set(-1, 'sync', 1)
  osc_loop_set(-1, 'playback_sync', 1)
  osc_loop_set(-1, 'use_feedback_play', 1)
  osc_get('selected_loop_num', None, '/loop_get')


def refresh_status():
  global state
  global browser
  state['midi']['devs_in'] = mido.get_input_names()
  state['midi']['devs_out'] = mido.get_output_names()
  browser.send_status(state)


def midi_set_in(dev):
  global browser
  port = mido.open_input(dev, callback=handle_midi_msg)
  state['midi']['dev_in'] = dev
  state['midi']['status'] = 'Connected'
  if browser != None:
    browser.send_status(state)

def midi_set_out(dev):
  global arturia_colors
  global browser
  arturia_colors = ArturiaColors(dev)
  state['midi']['dev_out'] = dev
  state['midi']['status'] = 'Connected'
  if browser != None:
    browser.send_status(state)


################# / APP METHODS ##################
# INIT
async def loop():
  osc_register_loop_count_updates()
  while True:
    await asyncio.sleep(3600)  # sleep forever


async def main():
  # MAIN
  global arturia_colors
  global browser
  global config
  global osc_client

  # MIDI
  #try:
  #  state['midi']['devices'] = mido.get_input_names()
  #  dev = state['midi']['devices'][0]
  #  midi_set_in(dev)
  #except Exception as e:
  #  print('No MIDI:', e)
  #  state['midi']['status'] = 'Not connected'
  #  state['midi']['msg'] = e.__str__()

  # OSC
  osc_client = SimpleUDPClient(
    config['osc_remote']['addr'],
    config['osc_remote']['port'],
  )
  dispatcher = Dispatcher()
  dispatcher.map('/ping', osc_recv_ping)
  dispatcher.map('/ping_get_loop_count', osc_recv_loop_count)
  dispatcher.map('/loop_get', osc_recv_loop_get)
  dispatcher.set_default_handler(osc_recv_default)
  osc_server = AsyncIOOSCUDPServer(
    (
      config['osc_local']['ip'],
      config['osc_local']['port'],
    ),
    dispatcher, asyncio.get_event_loop()
  )
  transport, protocol = await osc_server.create_serve_endpoint()

  # Timer to check for status
  timer = RepeatTimer(1, osc_check_status)
  timer.start()

  # WebSocket server
  await websockets.serve(
    ws_handler,
    config['http_ws_server']['addr'],
    config['http_ws_server']['port']
  )
  browser = Browser()

  # HTTP server
  async def index(request):
    return web.FileResponse('./public/index.html')
  app = web.Application()
  app.router.add_get('/', index)
  app.router.add_static('/', 'public')
  app.add_routes(routes)
  runner = web.AppRunner(app)
  await runner.setup()
  site = web.TCPSite(
    runner, config['http_server']['addr'],
    config['http_server']['port']
  )
  print(f"HTTP server listening on {config['http_server']['addr']}:{config['http_server']['port']}")
  await site.start()

  # Wait
  await loop()
  transport.close()


if __name__ == '__main__':
  asyncio.run(main())
