from threading import Timer

class RepeatTimer(Timer):
  # https://stackoverflow.com/questions/12435211/threading-timer-repeat-function-every-n-seconds
  def run(self):
    while not self.finished.wait(self.interval):
      self.function(*self.args, **self.kwargs)
