# The WebSocket messages that can be sent to the browser
import json
import websockets

class Browser:
  #def __init__(self, websocket):
  def __init__(self):
    self.WS_CONNECTIONS = set()
    pass
    #self.websocket = websocket


  def set_loop_count(self, loop_count):
    self._ws_send({ 'type': 'set_loop_count', 'value': loop_count })


  def touch_pad(self, loop_id):
    self._ws_send({
      'type': 'touch_pad',
      'value': loop_id,
    })


  def select_pad(self, loop_id):
    self._ws_send({
      'type': 'select_pad',
      'value': loop_id,
    })


  def mode_mute(self):
    self._ws_send({
      'type': 'mode_mute',
    })


  def send_status(self, status):
    s = status.copy()
    del s['last_ping']
    status['type'] = 'status'
    self._ws_send(s)


  # PRIVATE
  def _ws_send(self, msg):
    # msg is a dict
    websockets.broadcast(self.WS_CONNECTIONS, json.dumps(msg))
