# python-sooperlooper

This app is a controller/GUI for the SooperLooper. I created it because:

- I have Arturia MINILAB mkII which I would like to use to control SooperLooper
- I would like the long-press on the Arturia pad to trigger a specific action (e.g. overdub a loop), and I couldn't find a way to do it in the SooperLooper GUI
- I would like to highlight the Arturia pads using different colours to represent the state of the loop, e.g. loop is recorded, loop is recording, etc

Related blog article: http://konradp.gitlab.io/blog/post/com-loop-control/

```
pip3 install -r requirements.py
```


## Troubleshooting
```
ALSA lib conf.c:4005:(snd_config_hooks_call) Cannot open shared library libasound_module_conf_pulse.so (/usr/lib64/alsa-lib/libasound_module_conf_pulse.so: cannot open shared object file: No such file or directory)
ALSA lib seq.c:935:(snd_seq_open_noupdate) Unknown SEQ default
No MIDI
```
https://github.com/pygame/pygame/issues/3756
