config = {
  'http_server': {
    'addr': '0.0.0.0',
    'port': 9000,
  },
  'http_ws_server': {
    'addr': '0.0.0.0',
    'port': 9001,
  },
  # The SooperLooper instance
  'osc_remote': {
    'addr': '127.0.0.1',
    'port': 9951,
  },
  # python-sooperlooper OSC listener
  'osc_local': {
    'ip': '127.0.0.1',
    'port': 9000,
    'addr': '127.0.0.1:9000',
  },
}
