# osc_and_midi
This example combines MIDI and OSC.

Install requirements.
```
pip3 install -r requirements.py
```

This does:

- Press a pad to select a loop
- Selected loop is highlighted on Arturia
- Press sustain pedal to record the loop
- Long-press a pad to mute the loop
- Does not have GUI
