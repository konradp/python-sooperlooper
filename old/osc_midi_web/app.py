from aiohttp import web
from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient
import asyncio
import json
import mido
import threading as th
import time
import websockets

from arturia_colors import ArturiaColors
# server is the SooperLooper
# client is our app listener
# osc_client is our app client connecting to SL
# osc_server is our app listener

ip_server = '127.0.0.1'
port_server = 9951
ip_client = '127.0.0.1'
port_client = 9000
addr_client = f"{ip_client}:{port_client}"
DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'
COUNT_PADS = 8
DURATION_LONG_PRESS = 0.5
state_pads = [False]*COUNT_PADS
pad_timers = [None]*COUNT_PADS
state = {
  'pad_selected': None,
}
arturia_colors = None
osc_client = None
WS_CONNECTIONS = set()



##### Handlers: Websocket
async def ws_handler(websocket):
  WS_CONNECTIONS.add(websocket)
  print('handler')
  async for message in websocket:
    print('WS message:', message)

    try:
      await websocket.wait_closed()
    finally:
      WS_CONNECTIONS.remove(websocket)

def ws_send(msg):
  # msg is a dict
  websockets.broadcast(WS_CONNECTIONS, json.dumps(msg))



##### Handlers: OSC senders
def osc_send_ping():
  osc_client.send_message("/ping", [
    f"{addr_client}",
    "/ping"
  ])

def osc_set(param, value):
  # Sent a ping msg
  osc_client.send_message('/set', [ param, value ])

##### Handlers: OSC receivers
def osc_recv_ping(address, *args):
  print(f"{address}: {args}")

def handle_default(address, *args):
  print(f"DEFAULT {address}: {args}")


##### Handlers: MIDI

def handle_midi_msg(msg):
  pad = msg.note - 35 - 1 # 0, 1, 2
  if 0 <= pad <= 7 \
    and (msg.type in ['note_on', 'note_off' ]):
    if msg.type == 'note_on':
      pad_timers[pad] = th.Timer(DURATION_LONG_PRESS,
        check_longpress, None, { 'pad': pad }
      )
      pad_timers[pad].start()
      state_pads[pad] = True
      ws_send({
        'type': 'select_pad',
        'value': pad,
      })
    elif msg.type == 'note_off':
      pad_timers[pad].cancel()
      state_pads[pad] = False
      arturia_colors.reset_pad_colors()
      arturia_colors.set_pad_color(pad, 'blue')
      osc_set('selected_loop_num', pad)
  # DEBUG
  #ws_send(msg.dict())


## Other methods
def check_longpress(pad):
  if state_pads[pad]:
    print('long press on', pad)


async def loop():
  while True:
    await asyncio.sleep(3600)  # sleep forever


async def main():
  # MAIN
  global arturia_colors
  global osc_client

  arturia_colors = ArturiaColors(DEV)
  # MIDI
  port = mido.open_input(DEV, callback=handle_midi_msg)
  osc_client = SimpleUDPClient(ip_server, port_server)

  # OSC
  dispatcher = Dispatcher()
  dispatcher.map('/ping', osc_recv_ping)
  dispatcher.set_default_handler(handle_default)
  osc_server = AsyncIOOSCUDPServer((ip_client, port_client),
    dispatcher, asyncio.get_event_loop())
  transport, protocol = await osc_server.create_serve_endpoint()

  # WebSocket server
  await websockets.serve(ws_handler, "0.0.0.0", 9001)

  # HTTP server
  async def index(request):
    return web.FileResponse('./index.html')
  app = web.Application()
  app.router.add_get('/', index)
  app.router.add_static('/', 'public')
  runner = web.AppRunner(app)
  await runner.setup()
  site = web.TCPSite(runner, 'localhost', 9000)
  await site.start()


  # Wait
  await loop()
  transport.close()


if __name__ == '__main__':
  asyncio.run(main())
