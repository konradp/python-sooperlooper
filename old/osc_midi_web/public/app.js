console.log('test');
const ws = new WebSocket('ws://localhost:9001/');
window.addEventListener('load', onLoad);
ws.addEventListener('open', onWsOpen);

function onLoad() {
  console.log('load');
}

function onWsOpen() {
  console.log('WS opened');
  const msg = { type: 'play', column: 3 };
  ws.send(JSON.stringify(msg));
}

ws.addEventListener('message', ({ data }) => {
  const msg = JSON.parse(data);
  // do something with event
  console.log('msg from server:', msg);
});
