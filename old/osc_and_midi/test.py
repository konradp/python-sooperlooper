from pythonosc.dispatcher import Dispatcher
from pythonosc.osc_server import AsyncIOOSCUDPServer
from pythonosc.udp_client import SimpleUDPClient
import asyncio
import mido
import threading as th
import time

from arturia_colors import ArturiaColors
# server is the SooperLooper
# client is our app listener
# osc_client is our app client connecting to SL
# osc_server is our app listener

ip_server = '127.0.0.1'
port_server = 9951
ip_client = '127.0.0.1'
port_client = 9000
addr_client = f"{ip_client}:{port_client}"
DEV = 'Arturia MiniLab mkII:Arturia MiniLab mkII MIDI 1 20:0'
COUNT_PADS = 8
DURATION_LONG_PRESS = 0.5
state_pads = [False]*COUNT_PADS
pad_timers = [None]*COUNT_PADS
state = {
  'pad_selected': None,
}
arturia_colors = None
test = 'test1'
osc_client = None


def osc_send_ping():
  osc_client.send_message("/ping", [
    f"{addr_client}",
    "/ping"
  ])

def osc_set(param, value):
  osc_client.send_message('/set', [ param, value ])


def handle_ping(address, *args):
  print(f"{address}: {args}")


def handle_default(address, *args):
  print(f"DEFAULT {address}: {args}")


## MIDI methods
def check_longpress(pad):
  if state_pads[pad]:
    print('long press on', pad)


def handle_midi_msg(msg):
  pad = msg.note - 35 - 1 # 0, 1, 2
  if 0 <= pad <= 7 \
    and (msg.type in ['note_on', 'note_off' ]):
    if msg.type == 'note_on':
      pad_timers[pad] = th.Timer(DURATION_LONG_PRESS,
        check_longpress, None, { 'pad': pad }
      )
      pad_timers[pad].start()
      state_pads[pad] = True
    elif msg.type == 'note_off':
      pad_timers[pad].cancel()
      state_pads[pad] = False
      arturia_colors.reset_pad_colors()
      arturia_colors.set_pad_color(pad, 'blue')
      osc_set('selected_loop_num', pad)


async def loop():
  # Main program loop
  # TODO: Make this infinite loop
  for i in range(10000):
    print('a')
    await asyncio.sleep(1)


async def init_main():
  # MAIN
  global arturia_colors
  global osc_client
  test = 'test2'
  print('test:', test)

  arturia_colors = ArturiaColors(DEV)
  # MIDI
  port = mido.open_input(DEV, callback=handle_midi_msg)
  osc_client = SimpleUDPClient(ip_server, port_server)

  # OSC
  dispatcher = Dispatcher()
  dispatcher.map('/ping', handle_ping)
  dispatcher.set_default_handler(handle_default)
  osc_server = AsyncIOOSCUDPServer((ip_client, port_client),
    dispatcher, asyncio.get_event_loop())
  transport, protocol = await osc_server.create_serve_endpoint()
  await loop()
  transport.close()


if __name__ == '__main__':
  test = 'test3'
  asyncio.run(init_main())
