class Pads {
  constructor(canvas_id, settings={}) {
    // Variables
    this.canvas = document.getElementById(canvas_id);
    this.context = this.canvas.getContext('2d');
    this.canvas.width = window.innerWidth - 10;
    this.W = this.canvas.width;
    this.H = this.canvas.height;
    // Constructor
    this.draw();
  };


  draw() {
    const c = this.context;
    c.clearRect(0, 0, this.W, this.H);
    for (let pad = 1; pad <= this.countPads; pad++) {
      this.drawPad(pad);
    }
  };


  drawPad(pad) {
    const c = this.context;
    c.fillStyle = this.padState[pad-1].color;
    c.fillRect(
      pad*this.margin + (pad-1)*this.padW,
      20,
      this.padW,
      this.padH,
    );
  };


  resetPadColors() {
    for (let pad of this.padState) {
      pad.color = 'black';
    }
    this.draw();
  };


  setPadCount(count) {
    this.countPads = count;
    this.padState = [];
    for (let i=1; i<=this.countPads; i++) {
      this.padState.push({
        state: null,
        color: 'black',
      });
    }
    this.margin = this.W/30;
    this.padW = (this.W - (this.countPads+1)*this.margin)/this.countPads;
    this.padH = this.padW;
    this.draw();
  };


  setPadColor(pad, color) {
    this.padState[pad-1].color = color;
    this.draw();
  };


  modeMute() {
    // Enter the mute/unmute mode
    for (let pad of this.padState) {
      pad.color = 'yellow';
    }
    this.draw();
  };
}; // class Pads
