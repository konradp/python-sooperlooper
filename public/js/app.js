let IS_CONNECTED = false;
let pads = null;
let ws = null;
let state = {
  'midi': true,
  'osc': true,
};
let pages = {};
openWs();
window.addEventListener('load', onLoad);
//setInterval(getStatus, 1500)


/* API DOC */
// Send
// { type: test }
// { type: init }

// Receive
// { type: select_pad, value: ? }
// { type: touch_pad, value: red }
// { type: set_loop_count, value: 2 }
// { type: mode_mute }

function openPage(page) {
  console.log('PAGE', 'page-'+page);
  let div_page = document.querySelector('#page-' + page);
  div_page.style = 'background: red';
}

function openWs() {
  ws = new WebSocket('ws://localhost:9001/');
  ws.addEventListener('open', onWsOpen);
  ws.addEventListener('close', onWsClose);
}


function send(msg) {
  console.log('WS send:', msg);
  ws.send(JSON.stringify(msg));
}


function onLoad() {
  console.log('onLoad');
  pads = new Pads('canvas');
  console.log('pads done');

  // Pages
  pages = {
    'config': document.querySelector('#page-config'),
    'mixer': document.querySelector('#page-mixer'),
  };

  // Buttons: navbar
  document.querySelector('#btn-mixer').addEventListener('click', (e) => openPage(e.target.innerText));
  document.querySelector('#btn-config').addEventListener('click', () => {

  });

  // Buttons

  let btn_test = document.querySelector('#btn-test');
  btn_test.addEventListener('click', () => {
    send({ 'type': 'test' });
  });
  let btn_init = document.querySelector('#btn-init');
  btn_init.addEventListener('click', () => {
    send({ 'type': 'init' });
  });

  const btn_midi_refresh = document.getElementById('midi-refresh');
  btn_midi_refresh.addEventListener('click', (e) => getStatus());
  document.getElementById('midi-in').addEventListener('change', (e) => {
    send({ type: 'midi_set_in', value: e.target.value });
  });
  document.getElementById('midi-out').addEventListener('change', (e) => {
    send({ type: 'midi_set_out', value: e.target.value });
  });
}


function getStatus() {
  send({ 'type': 'get_status' });
};


function onWsOpen() {
  console.log('WebSockets established');
  IS_CONNECTED = true;
  const msg = { type: 'get_pad_count' };
  ws.send(JSON.stringify(msg));
  getStatus();
}


ws.addEventListener('message', ({ data }) => {
  // WS messages: receive
  const msg = JSON.parse(data);
  console.log('WS recv:', msg); // DEBUG

  switch (msg.type) {
    case 'mode_mute':
      pads.modeMute();
      break;
    case 'select_pad':
      pads.resetPadColors();
      pads.setPadColor(msg.value, 'blue');
      break;
    case 'set_loop_count':
      pads.setPadCount(msg.value);
      break;
    case 'status':
      // Received status update - update UI
      if (msg != state) {
        state = msg
        // MIDI
        //document.getElementById('midi').innerHTML = state.midi['status'];
        //document.getElementById('midi-msg').innerHTML = state.midi['msg'];
        document.getElementById('osc').innerHTML = state.osc['status'];
        // MIDI: input devices
        midi_ins = document.getElementById('midi-in');
        midi_ins.innerHTML = '';
        for (dev of state.midi['devs_in']) {
          let opt = document.createElement('option');
          opt.innerHTML = dev;
          if (state.midi.dev_in == dev) {
            opt.selected = 'true';
          }
          midi_ins.appendChild(opt);
        }
        // MIDI: output devices
        midi_outs = document.getElementById('midi-out');
        midi_outs.innerHTML = '';
        for (dev of state.midi['devs_out']) {
          let opt = document.createElement('option');
          opt.innerHTML = dev;
          if (state.midi.dev_out == dev) {
            opt.selected = 'true';
          }
          midi_outs.appendChild(opt);
        }
      }
      break;
    case 'touch_pad':
      pads.setPadColor(msg.value, 'red');
      break;
    default:
      console.log('ERROR: Unhandled msg:', msg);
  }
});


function onWsClose() {
  IS_CONNECTED = false;
  alert('Connection to app.py server lost');
  openWs();
}
